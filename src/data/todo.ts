export default function useTodos() {

    return {

    }
}

export const useFetch = async () => {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/todos/1')
        console.log(response);
        return response.json()
    } catch (error) {
        console.log(error);
    }
}