type NavItem = {
    id: number
    label: string
    route: string
};

export const links: NavItem[] = [
    {
        id: 0,
        label: 'Home',
        route: '/home'
    },
    {
        id: 1,
        label: 'Panel',
        route: '/panel'
    },
    {
        id: 2,
        label: 'About',
        route: '/about'
    },
    {
        id: 3,
        label: 'Todos',
        route: '/todos'
    },
];
