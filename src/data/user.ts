import type { IUser } from '@/interfaces/user';

export default function useUsers () {

    // Esta funcion genera un Key aleatorio en base hexadecimal
    function generateHexRandomNumber(length: number): string {
        const characters = "0123456789ABCDEF";
        let result = "";
        for (let i = 0; i < length; i++) {
            const randomIndex = Math.floor(Math.random() * characters.length);
            result += characters.charAt(randomIndex);
        }
        return result.toLowerCase();
    }

    function getUsers() {
        const users: IUser[] = [
            {
                _id: generateHexRandomNumber(7),
                name: 'Joaht',
                lastName: 'Vera',
                email: 'joaht@vue.com',
                birthDay: new Date('02/13/2000')
            },
            {
                _id: generateHexRandomNumber(7),
                name: 'Noemi',
                lastName: 'Un',
                email: 'noemi@vue.com',
                birthDay: new Date('07/11/2003')
            },
            {
                _id: generateHexRandomNumber(7),
                name: 'Fernando',
                lastName: 'Kumul',
                email: 'kumul@vue.com',
                birthDay: new Date('02/02/2000')
            },
            {
                _id: generateHexRandomNumber(7),
                name: 'Luis',
                lastName: 'Poke',
                email: 'poke@vue.com',
                birthDay: new Date('02/02/2000')
            },
            {
                _id: generateHexRandomNumber(7),
                name: 'Ana',
                lastName: 'Morales',
                email: 'ana@vue.com',
                birthDay: new Date('02/02/2000')
            }
        ]
        return users
    }

    return {
        getUsers
    }

}