export interface IUser {
    _id?: string,
    createdAt?: string
    email?: string
    lastName: string
    name?: string
    password?: string
    profilePictureUrl?: string
    role?: string
    updatedAt?: string
    birthDay?: Date
}