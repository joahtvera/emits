import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import About from '@/views/About.vue'
import Panel from '@/views/Panel.vue'
import Todos from '@/views/Todos.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/panel',
            name: 'panel',
            component: Panel
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/todos',
            name: 'todos',
            component: Todos
        },
    ]
})

export default router
